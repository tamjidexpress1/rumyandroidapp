package com.example.tamjid.mywebviewapp;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.view.KeyEvent;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    public WebView myWebView = null;
    private BluetoothAdapter mBluetoothAdapter;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private BluetoothLeScanner mLEScanner;
    private ScanSettings settings;
    private List<ScanFilter> filters;
    private boolean mScanning;
    private Handler mHandler;
    private static final long SCAN_PERIOD = 5000;
    private ArrayList<BluetoothDevice> mLeDevices;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[] {Manifest.permission.ACCESS_COARSE_LOCATION},PERMISSION_REQUEST_COARSE_LOCATION);
        }

        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "error_bluetooth_not_supported", Toast.LENGTH_SHORT).show();
            Log.d("tagg","bleadapter is null");
            finish();

        }
        mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
        settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .build();
        filters = new ArrayList<ScanFilter>();
        if (mLEScanner == null) {
            Log.d("tagg","scanner is null");
        }

        mHandler = new Handler();
        mLeDevices=new ArrayList<BluetoothDevice>();

        myWebView = (WebView) findViewById(R.id.webView);
        myWebView.setWebViewClient(new MyWebViewClient());
        myWebView.addJavascriptInterface(new WebAppInterface(this), "Android");


        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSaveFormData(false);
        //CookieManager.getInstance().setAcceptCookie(true);

       //webSettings.setAppCachePath("/data/data/com.example.tamjid.mywebviewapp/cache");
       // webSettings.setAppCacheEnabled(true);
        //webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
      //  webSettings.setDomStorageEnabled(true);
        // webSettings.setDatabaseEnabled(true);

                 myWebView.loadUrl("https://rumytechnologies.com/app/default/index");
                 //myWebView.loadUrl("https://kzubair.pythonanywhere.com/myblog");

       //myWebView.loadUrl("https://www.facebook.com");
    }
    private class MyWebViewClient extends WebViewClient{

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);


            return true;
        }
        // always verify the host - dont check for certificate
        /*@Override
        public void onReceivedSslError(final WebView view, final SslErrorHandler handler, final SslError error) {
            handler.proceed();
        }*/


    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) {
            myWebView.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }



public class WebAppInterface {
    Context mContext;

    /** Instantiate the interface and set the context */
    WebAppInterface(Context c) {
        mContext = c;
    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public void showToast(String toast) {
        /*RelativeLayout rel= (RelativeLayout) findViewById(R.id.relative);
        TextView tv=new TextView(MainActivity.this);
        tv.setText("text of lifx");
        rel.addView(tv);
        Toast.makeText(mContext, toast, Toast.LENGTH_LONG).show();
Log.d("api","toast shown");*/
scanTask task= new scanTask();
        task.execute();
    }
}
    public class scanTask extends AsyncTask<Void,Void,String>{

        @Override
        protected void onPreExecute() {
            if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)){
                this.cancel(true); }
        }

        @Override
        protected String doInBackground(Void... params) {
            scanLeDevice(true);
            //stop operation while scanning
            while (mScanning){}

           //build the JSON data containing device information
           if(mLeDevices!=null)
           {StringBuilder sb=new StringBuilder();
            sb.append("{\"devices\":[");
            for(int i=0;i<mLeDevices.size();i++) {

              BluetoothDevice device= mLeDevices.get(i);
              sb.append("{\"devicename\":\""+device.getName()+"\",\"deviceaddress\":\""+device.getAddress()+"\"}");
                if(!(i==(mLeDevices.size()-1))){
                    sb.append(",");

                }
            }
            sb.append("]}");
            String str=sb.toString();
            if(str.contains("\\"))
            { Log.d("sbchk","slash remains");}
               Log.d("sbchk",str);

            return str;}
            return null;
        }

        @Override
        protected void onPostExecute(String st) {
        if(st!=null){
            myWebView.postUrl("https://rumytechnologies.com/app/default/get_rumys",st.getBytes());
        }
        }
        private void scanLeDevice(final boolean enable) {
            if (enable) {


                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        mScanning = false;
                        mLEScanner.stopScan(mScanCallback);

                    }

                }, SCAN_PERIOD);
                Log.i("tagg","I'm in scan method");
                mScanning=true;
               mLEScanner.startScan(filters, settings, mScanCallback);

            }
            else {

                mScanning=false;
               mLEScanner.stopScan(mScanCallback);

            }
        }
        private ScanCallback mScanCallback = new ScanCallback() {

            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                Log.i("tagg", String.valueOf(callbackType));
                Log.i("tagg", result.toString());
                Log.i("tagg", "I'm in");

                BluetoothDevice device = result.getDevice();
                if(!mLeDevices.contains(device)) {
                    mLeDevices.add(device);}

            }
        };


    }
}